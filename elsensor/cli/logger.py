"""
Command line tool to log sensor data.
"""

# python core import
from sys import exit
from time import sleep
from datetime import datetime
from struct import unpack

# 3rd party imports (installed with pip or conda)
from click import (command, secho, pause, option)

# Local imports
from ..sensor import Sensor, Status


@command()
@option('--port', required=True, type=str,
        help='Port COMID (e.g. COM12).')
@option('--timeout', default=250, type=int,
        help='COM timeout in milliseconds, default is 250')
@option('--period', default=4.0, type=float,
        help='log period in seconds, default is 4.0')        
@option('--debug', default=False, is_flag=True,
        help='Enable dump of written and read data, default is False')
def main(port, timeout, period, debug):
    try:
        sensor = Sensor(port, timeout, debug)

        # Print header
        secho("concentration,temperature,status,alarm,error,timestamp")

        # Repeat until CTRL+C
        mustExit = False
        while not mustExit:
            try:
                # Log data peridodically
                log_sensor_data(sensor)
                sleep(period)

            except KeyboardInterrupt:
                # User pressed CTRL+C
                mustExit = True

            except Exception:
                # Something bad happened, forward the Exception
                raise

    except Exception as err:
        # Exit with an error code
        secho("Error: {}".format(err))
        pause()
        exit(1)


def log_sensor_data(sensor):
    """Get one data and log it to the standard output"""

    id = 0x00
    maskH = 0x00
    maskL = 0x2F
    # Send the command
    if Status.OK == sensor.write(0x30, bytearray([id, maskH, maskL])):
        # Read the answer
        status, answer = sensor.read()
        # Check the answer status
        if Status.OK == status:
            index = 6
            status = "{:08b}".format(answer[index])
            index += 1

            alarm = "{:08b}".format(answer[index])
            index += 1

            nbError = answer[index]
            index += 1

            if nbError:
                for i in range(nbError):
                    # ignore, todo: add error info in the log
                    # format_message("Error byte #{}".format(i),
                    #    answer[index])
                    index += 1

            # gas reading
            gasReading, = unpack('>i', answer[index:index+4])
            gasReading = gasReading // 100
            index += 4

            # temperature reading
            temperature = answer[index]
            if temperature != 0xFF:
                temperature -= 127
            index += 1

            # Print the data and its timestamp
            timestamp = datetime.utcnow()
            secho("{},{},{},{},{},{}".format(
                gasReading, temperature, status,
                alarm, nbError, timestamp
            ))


if __name__ == "__main__":
    main()
