"""
Command line tool that prompts for user choice.
"""

# python core import
from sys import exit
from time import sleep
from datetime import datetime
from struct import unpack, pack

# 3rd party imports (installed with pip or conda)
from click import (command, echo, secho, progressbar,
                   prompt, pause, option)

# Local imports
from ..sensor import Sensor, Status


@command()
@option('--port', required=True, type=str,
        help='Port COMID (e.g. COM12).')
@option('--timeout', default=250, type=int,
        help='COM timeout in milliseconds, default is 250')
@option('--action', default=0, type=int,
        help='Action to perform without prompting, default is 0 (prompting)')
@option('--debug', default=False, is_flag=True,
        help='Enable dump of written and read data, default is False')
def main(port, timeout, action, debug):
    try:
        sensor = Sensor(port, timeout, debug)

        if action:
            handle_user_input(action, sensor)
        else:
            action = get_user_input()
            while action:
                handle_user_input(action, sensor)
                action = get_user_input()
    except Exception as err:
        # Exit with an error code
        secho("Error: {}".format(err))
        pause()
        exit(1)



def get_user_input():
    echo("")
    echo("Action list:")
    echo("  0: Exit")
    echo("  1: Show sensor constant information")
    echo("  2: Show sensor changing information")
    echo("  3: Show sensor data")
    echo("  4: Show sensor calibration data")
    echo("  5: Set sensor parameter")
    echo("  6: Synchronize RTC")
    echo("  7: Fresh-Air Calibrate")
    echo("  8: Zero Calibrate")
    echo("  9: Span Calibrate")
    echo(" 10: Reset")
    echo(" 11: Stop")

    echo("")
    user_choice = prompt('Please select an action', type=int)
    return user_choice


def handle_user_input(actionID, sensor):
    if actionID == 1:
        show_sensor_constant_information(sensor)
    elif actionID == 2:
        show_sensor_changing_information(sensor)
    elif actionID == 3:
        show_sensor_data(sensor)
    elif actionID == 4:
        show_sensor_cal_data(sensor)
    elif actionID == 5:
        set_sensor_parameter(sensor)
    elif actionID == 6:
        synchronize_rtc(sensor)
    elif actionID == 7:
        fresh_air_calibrate(sensor)
    elif actionID == 8:
        zero_calibrate(sensor)
    elif actionID == 9:
        span_calibrate(sensor)
    elif actionID == 10:
        reset(sensor)
    elif actionID == 11:
        stop(sensor)


def format_message(msg, value, total_size=45):
    strValue = str(value)
    if strValue[-1] == "\0":
        strValue = strValue[:-1]
    echo(
        "{}:{}{}".format(
            msg,
            " " * max(1, total_size - len(msg) - len(strValue)),
            strValue
        )
    )


def _disable_write_protection(sensor):
    # Disable write protection"
    result = False
    if Status.OK == sensor.write(0xA0, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            result = True
    return result


def _enable_write_protection(sensor):
    # Enable write protection"
    result = False
    if Status.OK == sensor.write(0xA0, bytearray([0x01])):
        status, answer = sensor.read()
        if Status.OK == status:
            result = True
    return result


def show_sensor_constant_information(sensor):
    echo("")
    # Model Name
    if Status.OK == sensor.write(0x10):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Model Name", answer[6:-3].decode("utf-8"))

    # Product Name
    if Status.OK == sensor.write(0x11):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Product Name: ", answer[6:-3].decode("utf-8"))

    # Sensor Name
    if Status.OK == sensor.write(0x35, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Sensor Name", answer[6: -3].decode("utf-8"))

    # Serial number
    if Status.OK == sensor.write(0x13):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Serial Number", answer[6:-3].decode("utf-8"))

    # FW version
    if Status.OK == sensor.write(0x12):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Firmware Version", answer[6:-3].decode("utf-8"))

    # Data Format
    if Status.OK == sensor.write(0x31, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            units = {
                "0": "ppm", "1": "%", "2": "ppb",
                "39": "%LEL", "40": "%vol"
            }
            format_message("Data unit",
                           units.get(str(answer[6]), "ERROR"))
            format_message("Data resolution",
                           "{}.10^{}".format(answer[7], answer[8]))
            format_message("Parameter mask low",
                           "{:08b}".format(answer[10]))
            format_message("Parameter mask high",
                           "{:08b}".format(answer[9]))

    # Sensor information
    if Status.OK == sensor.write(0x15):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Number of sensors", answer[6])
            val, = unpack('>H', answer[7:9])
            format_message("Installed sensor mask", "{:016b}".format(val))

    # Production date
    if Status.OK == sensor.write(0x37, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Production date", "{:02d}/{:02d}/{}".format(
                answer[7], answer[8], 2000+answer[6]
            ))

    # Calibration time
    if Status.OK == sensor.write(0x43, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            calibrationTime, = unpack('>H', answer[6:8])
            format_message("Calibration time [s]", calibrationTime)


def show_sensor_changing_information(sensor):
    echo("")
    # Running mode
    if Status.OK == sensor.write(0x17):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message("Running mode",
                           "Application" if answer[6] else "Bootload")
    # Runtime
    if Status.OK == sensor.write(0x14):
        status, answer = sensor.read()
        if Status.OK == status:
            ans, = unpack('>I', answer[6:-3])
            format_message("Runtime", "{} seconds".format(ans))

    # Parameters
    maskHigh = 0x08
    maskLow = 0x17

    if Status.OK == sensor.write(0x33, bytearray([0x00, maskHigh, maskLow])):
        status, answer = sensor.read()
        if Status.OK == status:
            # sensor span
            span, = unpack('>I', answer[6:10])
            span = span / 100
            format_message("Parameter SPAN", span)
            # sensor low
            low, = unpack('>I', answer[10:14])
            low = low / 100
            format_message("Parameter LOW", low)
            # sensor high
            high, = unpack('>I', answer[14:18])
            high = high / 100
            format_message("Parameter HIGH", high)
            # sensor Over range
            overrange, = unpack('>I', answer[18:22])
            overrange = overrange / 100
            format_message("Parameter OVERRANGE", overrange)
            # sensor drift
            drift, = unpack('>I', answer[22:26])
            drift = drift / 100
            format_message("Parameter DRFIT", drift)

    # RCI parameters
    if Status.OK == sensor.write(0xE1, bytearray([0x00, 0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            ans, = unpack('>I', answer[8:-3])
            format_message("RCI baseline", "{} ppm".format(ans))
    if Status.OK == sensor.write(0xE1, bytearray([0x00, 0x01])):
        status, answer = sensor.read()
        if Status.OK == status:
            ans, = unpack('>I', answer[8:-3])
            format_message("RCI period", "{} days".format(ans))
    if Status.OK == sensor.write(0xE1, bytearray([0x00, 0x02])):
        status, answer = sensor.read()
        if Status.OK == status:
            ans, = unpack('>I', answer[8:-3])
            format_message("RCI total periods", "{}".format(ans))

    # RTC
    if Status.OK == sensor.write(0x38, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            format_message(
                "RTC",
                "{:02d}/{:02d}/{} {:02d}:{:02d}:{:02d}".format(
                    answer[7], answer[8], 2000+answer[6],
                    answer[9], answer[10], answer[11],
                )
            )


def show_sensor_data(sensor):
    echo("")

    # Data mask
    id = 0x00
    maskHigh = 0x00
    maskLow = 0x2F
    if Status.OK == sensor.write(0x30, bytearray([id, maskHigh, maskLow])):
        status, answer = sensor.read()
        if Status.OK == status:
            index = 6
            format_message("Status", "{:08b}".format(answer[index]))
            index += 1

            # sensor alarm
            # b0 : Overrange
            # b1 : Reserved
            # b2 : Time not synchronized
            # b3 : High
            # b4 : Low
            # b5 : Reserved
            # b6 : Reserved
            # b7 : Drift

            format_message("Alarm", "{:08b}".format(answer[index]))
            index += 1

            nbError = answer[index]
            format_message("Error number", "{}".format(nbError))
            index += 1

            if nbError:
                for i in range(nbError):
                    format_message("Error byte #{}".format(i), answer[index])
                    index += 1

            # gas reading
            gasReading, = unpack('>i', answer[index:index+4])
            gasReading = gasReading // 100
            format_message("Gas reading", gasReading)
            index += 4

            # temperature reading
            temperatureRaw = answer[index]
            if temperatureRaw != 0xFF:
                format_message("Temperature", temperatureRaw - 127)
            index += 1


def show_sensor_cal_data(sensor):
    echo("")
    if Status.OK == sensor.write(0x46, bytearray([0x00])):
        status, answer = sensor.read()
        if Status.OK == status:
            nbCalChannels = answer[6]  # Zero and span (=2 channels)

            for i in range(1, 1 + nbCalChannels):
                idx = 7 + (i-1) * 16

                # value
                calValue, = unpack('>I', answer[idx:idx+4])
                calValue = calValue // 100
                format_message("Cal Point #{} value".format(i), calValue)

                # raw Value
                rawValue, = unpack('>H', answer[idx+4:idx+6])
                format_message(
                    "Cal Point #{} raw value #1".format(i),
                    "{} ({:.2f}mV)".format(
                        rawValue, rawValue * 2800 / (16 * 16384)
                    )
                )
                rawValue, = unpack('>H', answer[idx+6:idx+8])
                format_message(
                    "Cal Point #{} raw value #2".format(i),
                    "{} ({:.2f}mV)".format(
                        rawValue, rawValue * 2800 / (16 * 16384)
                    )
                )

                # temperature
                temperature = answer[idx+8]
                format_message(
                    "Cal Point #{} temperature".format(i),
                    temperature-127
                )

                # date
                format_message(
                    "Cal Point #{} date".format(i),
                    "{:02d}/{:02d}/{} {:02d}:{:02d}:{:02d}".format(
                        answer[idx+11], answer[idx+12], 2000+answer[idx+10],
                        answer[idx+13], answer[idx+14], answer[idx+15],
                    )
                )

            if Status.OK == sensor.write(0xC0, bytearray([0x00])):
                status, answer = sensor.read()
                if Status.OK == status:
                    # Show last errors
                    calErrors, = unpack('>H', answer[6:8])
                    calType = answer[8]
                    calTypeTxt = "SPAN" if calType == 1 else "ZERO"
                    format_message(
                        "Last Calib Error Code", "{} ({})".format(
                            calErrors, calTypeTxt
                        )
                    )


def _calibrate(sensor, txt, cal):
    echo("")

    user_value = -1
    while user_value < 0:
        user_value = prompt(
            'Please enter a {} calibration time or 0 to abort'.format(txt),
            type=int
        )
    if not user_value:
        return

    cal_time = user_value

    # Disable write protection
    if not _disable_write_protection(sensor):
        secho("Error could not disable write protection", fg="red")
        return

    # Prepare calibration, gas should be applied now
    status = sensor.write(0xA1, bytearray([0x01, 0x00, cal, 0x80]))
    if Status.OK == status:
        status, answer = sensor.read()
    if Status.OK != status:
        secho("Could not prepare {} calibration ({})".format(txt, status),
              fg="red")
        return

    # Waiting for the gas to be applied and value to be stable
    abort = False
    label = "{} Calibrating{}".format(txt, " "*(10 - len(txt)))
    with progressbar(length=cal_time, label=label) as bar:
        counter = 0.0
        increment = 0.5
        bar.update(0)
        try:
            while counter <= cal_time:
                sleep(increment)
                bar.update(increment)
                counter += increment
        except KeyboardInterrupt:
            abort = True

    if abort:
        # Abort requested
        status = sensor.write(0xA1, bytearray([0x01, 0x00, cal, 0x81]))
        if Status.OK == status:
            status, answer = sensor.read()
        if Status.OK != status:
            secho("Could not abort calibration ({})".format(status),
                  fg="red")
            return
        else:
            secho("Calibration CANCELED", fg="yellow")
    else:
        # Trigger computation and storage (readings must be stable)
        status = sensor.write(0xA1, bytearray([0x01, 0x00, cal, 0x00]))
        if Status.OK == status:
            status, answer = sensor.read()
        if Status.OK != status:
            secho("Could not start calibration ({})".format(status),
                  fg="red")
            return

        calTimeout, =  unpack('>H', answer[6:8])
        calTimeout = int(calTimeout / 1000.0)

        # Wait for the storage to happen
        label = "Validating{}".format(" " * 12)
        with progressbar(length=calTimeout, label=label) as bar:
            counter = 0.0
            increment = 0.5
            bar.update(0)
            while counter <= calTimeout:
                sleep(increment)
                bar.update(increment)
                counter += increment

        # Check that the calibration coefficients storage went well
        status = sensor.write(0xA1, bytearray([0x01, 0x00, cal, 0x83]))
        if Status.OK == status:
            status, answer = sensor.read()
        if Status.OK != status:
            secho("Could not check calibration ({})".format(status),
                  fg="red")
            return

        cal_status, = unpack('>H', answer[6:8])
        if cal_status:
            secho("Calibration SUCCEED", fg="green")
        else:
            status2 = sensor.write(0xC0, bytearray([0x00]))

            if Status.OK == status2:
                status2, answer2 = sensor.read()
                calErrors, = unpack('>H', answer2[6:8])
                # calType should match parameter "cal"
                # calType = answer2[8]

                secho("Calibration FAILED (Error {} on {} calib)".format(
                    calErrors, txt), fg="red")
            else:
                secho("Calibration FAILED ({})".format(
                    "Unknown err!"), fg="red")

    if not _enable_write_protection(sensor):
        secho("Warning could not enable write protection", fg="yellow")
        return


def fresh_air_calibrate(sensor):
    return _calibrate(sensor, "Fresh air", 0x03)


def zero_calibrate(sensor):
    return _calibrate(sensor, "Zero", 0x00)


def span_calibrate(sensor):
    return _calibrate(sensor, "Span", 0x01)


def synchronize_rtc(sensor):
    echo("")

    if not _disable_write_protection(sensor):
        secho("Error could not disbale write protection", fg="red")
        return

    # Current time
    now = datetime.now()
    # Synchronize the RTC
    status = sensor.write(0x82, bytearray([
        now.year-2000, now.month, now.day,
        now.hour, now.minute, now.second
    ]))
    if Status.OK == status:
        status, answer = sensor.read()

    if Status.OK == status:
        secho("Successfully set the RTC to {}".format(now),
              fg="green")
    else:
        secho("Failed to set the RTC to {} ({})".format(now, status),
              fg="red")

    if not _enable_write_protection(sensor):
        secho("Warning could not enable write protection", fg="yellow")


def reset(sensor):
    echo("")

    if not _disable_write_protection(sensor):
        secho("Error could not disbale write protection", fg="red")
        return

    # Change the mode
    status = sensor.write(0xA6, bytearray([0x01]))
    if Status.OK == status:
        secho("first com ok", fg="green")
        status, answer = sensor.read()

    if Status.OK == status:
        secho("Successfully reset the sensor", fg="green")
    else:
        secho("Failed to reset the sensor ({})".format(status), fg="red")

    sleep(2)

    if not _enable_write_protection(sensor):
        secho("Warning could not enable write protection", fg="yellow")


def stop(sensor):
    echo("")

    if not _disable_write_protection(sensor):
        secho("Error could not disbale write protection", fg="red")
        return

    # Change the mode
    status = sensor.write(0xA6, bytearray([0x02]))
    if Status.OK == status:
        status, answer = sensor.read()

    if Status.OK == status:
        secho("Successfully stop the sensor", fg="green")
    else:
        secho("Failed to stop the sensor ({})".format(status), fg="red")

    sleep(0.5)

    if not _enable_write_protection(sensor):
        secho("Warning could not enable write protection", fg="yellow")


def get_sensor_parameter_user_input(sensor):
    echo("")
    echo("What parameters to set:")
    echo("  0: Exit")
    echo("  1: SPAN")
    echo("  2: LOW")
    echo("  3: HIGH")

    echo("")
    user_choice = prompt('Please select an action', type=int)
    return user_choice


def set_sensor_parameter(sensor):
    user_choice = get_sensor_parameter_user_input(sensor)
    while user_choice:
        parameterID = user_choice-1
        handle_user_input_set_parameter(parameterID, sensor)
        user_choice = get_sensor_parameter_user_input(sensor)


def handle_user_input_set_parameter(parameterID, sensor):
    user_value = -1
    while user_value < 0:
        user_value = prompt(
            'Please enter the new value or 0 to abort', type=int)
    if not user_value:
        return

    maskHigh = 0x00
    maskLow = 2 ** parameterID

    value = pack('>I', int(user_value * 100))

    if not _disable_write_protection(sensor):
        secho("Error could not disbale write protection", fg="red")
        return

    if Status.OK == sensor.write(0x80, bytearray([0x00, maskHigh, maskLow,
                                                  value[0], value[1],
                                                  value[2], value[3]])):
        status, answer = sensor.read()
    if Status.OK == status:
        secho("Parameter was set correctly", fg="green")
    else:
        secho("Parameter was not set correctly ({})".format(status),
              fg="yellow")

    if not _enable_write_protection(sensor):
        secho("Warning could not enable write protection", fg="yellow")


if __name__ == "__main__":
    main()
