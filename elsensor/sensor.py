# -*- coding: utf-8 -*-
"""
Sensor Module

Copyright (c) 2016-2021 ELichens All rights reserved.
"""

# python core import
from enum import Enum
from time import sleep
import struct

# 3rd party imports (installed with pip or conda)
from serial import Serial

# Local imports
from .crc import CRC16


# Protocol constants
SOP = 0x7B
EOP = 0x7D
VER = 0x59
LENGTH_BASE = 6  # LENGTH_BASE + size of payload = length byte

# Format for endiannes conversions
FMT_INT = '>I'
FMT_SHORT = '>H'


# All the protocol status codes
class Status(Enum):
    OK = 0
    TIMEOUT = 1
    WRONG_CRC = 2
    WRONG_FORMAT = 3
    WRITE_ERROR = 4
    ERROR_PACKET = 5


class Sensor(object):
    """Implements a Berry Sensor using SDCS protocol"""

    def __init__(self, portId, timeout=250, debug=False):
        super(Sensor, self).__init__()

        self.debug = debug
        self.index = 0

        # Create the Serial object and open it
        self.serial = Serial()
        self.open(portId, 57600, timeout)
        sleep(1.0)


    def buildPacket(self, cmd, payload=bytearray()):
        """Build a packet using SDCS format """

        # Index is increment every new packet (uint16 data)
        index = struct.pack(FMT_SHORT, int(self.index))
        self.index = self.index + 1
        if self.index > 65535:
            self.index = 0

        # packet size = lenght of payload + lenght of footer
        size = len(payload) + LENGTH_BASE
        packet = bytearray([SOP, VER, size, index[0], index[1], cmd])
        packet += payload
        crc_h, crc_l = CRC16.computeBytes(packet)
        packet += bytearray([crc_h, crc_l, EOP])

        return packet

    def read(self):
        """Read a packet using SDCS format"""

        in_packet = False
        packet = bytearray()
        while 1:
            data = self.serial.read(1)
            if not len(data):
                return self._read_return(Status.TIMEOUT, packet)

            byte = data[0]
            if byte == SOP:
                in_packet = True
                packet.append(byte)

            elif in_packet:
                packet.append(byte)
                if len(packet) > 3 and len(packet) == (packet[2] + 3):
                    # We reached the theoritical end of the packet
                    in_packet = False

                    # Check the stop byte
                    if byte != EOP:
                        return self._read_return(Status.WRONG_FORMAT, packet)

                    # Check the CRC
                    msb, lsb = CRC16.computeBytes(packet[:-3])
                    if msb != packet[-3] or lsb != packet[-2]:
                        return self._read_return(Status.WRONG_CRC, packet)

                    # Check error packet
                    if 0x71 == packet[5]:
                        return self._read_return(Status.ERROR_PACKET, packet)

                    # OK
                    return self._read_return(Status.OK, packet)

    def open(self, port, baudrate, timeout):
        """Open the serial port comm."""

        self.serial.port = port
        self.serial.baudrate = baudrate
        self.serial.timeout = timeout / 1000.0
        self.serial.open()

    def close(self):
        """Close the serial port comm."""

        self.serial.close()

    def write(self, cmd, payload=bytearray()):
        """Write a command and its payload to the serial port."""

        self.serial.reset_input_buffer()
        frame = self.buildPacket(cmd, payload)
        if self.debug:
            print('\nWRITE ' + self.frameToString(frame))

        written = self.serial.write(frame)
        return Status.OK if (len(frame) == written) else Status.WRITE_ERROR

    def _read_return(self, status, packet):
        """Internal function to be used when returning from the 'read'."""

        if self.debug:
            print('READ ' + self.frameToString(packet))

        # Send a copy and cleanup for the next one
        return status, bytearray(packet)

    @staticmethod
    def frameToString(frame):
        """Function to convert the frame to a readable format."""

        s = '[ '
        for c in frame:
            s += "{} ".format(hex(c))
        s += ']'
        return s
