# pyELSensor

A Python library to deal with eLichens sensors.

## Samples

Note, we suppose that the sensor is on serial port COM7.

This will log data the in console:

    python -m elsensor.cli.logger --port COM7

This will log data to the myfilename.csv:

    python -m elsensor.cli.logger --port COM7 > myfilename.csv

This will start a command line interface where you can check all protocol commands:

    python -m elsensor.cli.prompt --port COM7

For any of the samples you can use:

- --help to get information about the command line syntax.
- --debug to show protocol frames in hexadecimal format.
